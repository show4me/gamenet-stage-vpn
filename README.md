# gamenet-stage-vpn
###This scripts allow local user to start ssh-based VPN to a Develop server

Clone repository to local machine.
Execute ./start.sh
Use listed domains to open in a browser and connect to databases.
Press any key to stop VPN.

## Usage
* See the list of available hosts just afrer start VPN
* Put certain host:port into browser search field to open a web page
* (or) Use host:port inside a local database client to connect to a database