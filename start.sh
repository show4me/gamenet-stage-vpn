#!/usr/bin/env bash

IP_P1=18.169.213.252 #
IP_P2=18.169.213.252 #
IP_P3=18.169.213.252 #
IPS=("${IP_P1}" "${IP_P2}" "${IP_P3}")

DOCKER_PORT_P1=14997
SERVER_NAMES=("STAGE-GAMENET" "STAGE-GAMENET" "STAGE-GAMENET") #Just labels

HERE=$(pwd -P)
SSH_CONNECT_TIMEOUT=5
DOCKER_API_PORT=2375

# configure GIT to not to asc each time for git password
#git config --global credential.helper 'store --file ~/.git-credentials' # Use in case of permanently store credentials
git config --global credential.helper 'cache --timeout 30000'

(("DOCKER_PORT_P2=DOCKER_PORT_P1+1"))
(("DOCKER_PORT_P3=DOCKER_PORT_P1+2"))
DOCKER_PORTS=("${DOCKER_PORT_P1}" "${DOCKER_PORT_P2}" "${DOCKER_PORT_P3}")
(("STARTING_FORWARD_PORT=DOCKER_PORT_P1+3"))


kill_ssh_sessions() {
    ARRAY_LEN=${#IPS[@]}
    for ((i=0;i<${ARRAY_LEN};i++))
    do
        if [[ "$OSTYPE" == "linux-gnu" ]] #Linux
        then
            ps aux | grep ssh | grep ${IPS[i]} | grep '\-fN' | sed -e's/  */ /g' | cut -f2 -d' ' | sed ':a;N;$!ba;s/\n/ /g' | xargs --no-run-if-empty kill
        elif [[ "$OSTYPE" == "darwin"* ]] #MacOS
        then
            ps aux | grep ssh | grep ${IPS[i]} | grep '\-fN' | sed -e's/  */ /g' | cut -f2 -d' ' | sed ':a;N;$!ba;s/\n/ /g' | xargs kill
        else
            echo "[ERROR] The \"${OSTYPE}\" operational system is not supported. Exiting."
            exit 1
        fi
    done
}

# kill inactive ssh sessions
kill_ssh_sessions

## --------  docker API, servers  --------

ARRAY_LEN=${#DOCKER_PORTS[@]}
echo "** (Start docker API tunnels, if you have access):"
for ((i=0;i<${ARRAY_LEN};i++))
do
    ssh -o ConnectTimeout=${SSH_CONNECT_TIMEOUT} -p22 ${IPS[i]} -L ${DOCKER_PORTS[i]}:127.0.0.1:${DOCKER_API_PORT} -fN
    RES=$?
    if [[ "$RES" == "0" ]]
    then
        echo "** $i) Use 127.0.0.1:${DOCKER_PORTS[i]} to attach your local Portainer to a ${SERVER_NAMES[i]} docker node."
    else
        echo "** $i) Error connecting to " ${IPS[i]}"."
    fi
done

echo
echo "** (Update and start VPN to the remote services):"
cd "$(dirname ${0})" || echo "[WARNING]: Can't cd to VPN script runtime directory."
pwd
echo "** (GIT, pull):"
git pull
echo "** (Starting VPN):.."
./vpn.sh ${STARTING_FORWARD_PORT} ${IP_P1} ${IP_P2} ${IP_P3} || bash -c "echo; echo '** [ERROR] Error creating
ADDITIONAL VPN hosts. Press <enter> to exit.' && read"

kill_ssh_sessions

cd ${HERE} || echo "[WARNING]: Can't cd to original console directory."
echo
echo "**********"
echo "** Buy! **"
echo "**********"
echo

# An example of create tulnnel with console and ssh manually:
# ssh -p22 35.177.61.126 -L 2691:127.0.0.1:2375 -N -v
