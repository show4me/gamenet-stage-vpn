#!/usr/bin/env bash
# COMPLETTE LOCAL SETUP FOR STAGE SERVER
SSH_CONNECT_TIMEOUT=5
IP_P1=$2
IP_P2=$3
IP_P3=$4
STARTING_FORWARD_PORT=${1:-13000}

ENVT=stage-gamenet

#LINE FORMAT:  'local_name'  'remote_service:remote_service_port' 'path'
SERVICE_NAMES=(
  'bpe'             'bpe:80' '/swagger/'
  'config'          'config:80' '/swagger/'
  'content'         'content:80' '/swagger/'
  'crowdfunding'    'crowdfunding:80' '/swagger/'
  'document-templates' 'document-templates:80' '/swagger/'
  'file-mover'      'file-mover:80' '/swagger/'
  'file-uploader'   'file-uploader:80' '/swagger/'
  'inform'          'inform:1234' ''
  'inform'          'inform:80' '/swagger'
  'localise'        'localise:80' '/swagger/'
  'main-front'      'main-front:80' '/'
  'media-converter' 'media-converter:80' '/swagger/'
  'messenger'       'messenger:80' '/swagger/'
  'mongo_01'        'mongo_01:27017' ''
  'mongo_02'        'mongo_02:27017' ''
  'mongo_03'        'mongo_03:27017' ''
  'mysql'           'mysql:3306' ''
  'notification'    'notification:80' '/swagger/'
  'oms'             'oms:80' '/swagger/'
  'payment-service' 'payment-service:80' '/swagger/'
  'pls'             'pls:80' '/swagger/'
  'portainer'       'portainer_portainer:9000' '/'
  'postgres-rds'    'stage-db-restored.cxdfpknsitls.eu-west-2.rds.amazonaws.com:5432' '' # Этот  рабочий
#  'postgresql-slave' 'stage-pg.cxdfpknsitls.eu-west-2.rds.amazonaws.com:5432' '' # Интсанс удален из AWS, не используем
  'products'        'products:80' '/swagger/'
  'rabbit-web'      'rabbitmq:15672' '/'
  'rds'             'rds:80' '/swagger/'
  'redis'           'redis:6379' ''
  'redis-web-interface' 'redis-web-interface:8081' '/'
  'russian_db'      'rus-tunnel:5432' ''
  'sphinx'          'sphinx:9306' ''
  'sso'             'sso:80' '/swagger/'
  'statistic'       'statistic:80' '/swagger/'
  'system-service'  'system-service:80' '/swagger/'
  'ums'             'ums:80' '/swagger/'
  'payment-service-2' 'payment-service-2:80' '/swagger/'
#  'admin-platform'  'admin-platform:80' '/'
#  'blank'           'blank-service:80' '/swagger/'
#  'graylog'         'graylog:9000' '/'
#  'test-platform'   'test-platform:80' '/swagger/'
#  'ups'             'ups:80' '/swagger/'
)

DOMAIN="${ENVT}.vpn"
SSH_PORT='2222'
SSH_USER='develop'
ARRAY_LEN=${#SERVICE_NAMES[@]}

clear_hosts() {
    if [[ "$OSTYPE" == "linux-gnu" ]]
    then
        sudo sed -i -r "/(\.|\s)${ENVT}\.vpn/d" /etc/hosts
    elif [[ "$OSTYPE" == "darwin"* ]]
    then
        sudo sed -i '' -E "/(\.|[[:space:]])${ENVT}\.vpn/d" /etc/hosts
    fi
}

gen_hosts() {
    HOSTS_STRING='127.0.0.1 '
    for ((i=0;i<ARRAY_LEN;i++))
    do
        HOSTS_STRING="${HOSTS_STRING}${SERVICE_NAMES[i]}.${DOMAIN} "
    done
}

ssh_connect() {
    FORWARD_PORT=$STARTING_FORWARD_PORT
    SSH_ARGS=("-o" "StrictHostKeyChecking=no" "-o" "ConnectTimeout=${SSH_CONNECT_TIMEOUT}")
    echo '==========================================================='
    echo '*** The list of tunneling items HOST:PORT'
    echo '==========================================================='
    STR=""
    for ((i=0;i<$((ARRAY_LEN/3));i++))
    do
        II=$((i*3))
        TNAME="${SERVICE_NAMES[II]}"
        TREMT="${SERVICE_NAMES[$((II+1))]}"
        TPATH="${SERVICE_NAMES[$((II+2))]}"
        SSH_ARGS+=("-L")
        SSH_ARGS+=("${TNAME}.${DOMAIN}:${FORWARD_PORT}:${TREMT}")
        STR=$(printf "%s\\\n" "${STR}http://${TNAME}.${DOMAIN}:${FORWARD_PORT}${TPATH}")

        ((FORWARD_PORT++))
    done
    echo -e ${STR} | sort
    echo '==========================================================='
    echo
    echo "*** (Next lines are getting from a REMOTE system):"
    SSH_ARGS+=("${SSH_USER}@${ENVT_HOST}")
    SSH_ARGS+=("-p${SSH_PORT}")
    ssh "${SSH_ARGS[@]}"
}

# Delete all VPN lines from a /etc/hosts
clear_hosts
sleep 1
# Add lines to a local /etc/hosts:
gen_hosts
echo "# ${ENVT}.vpn part. Cleared automatically after end of vpn script.
${IP_P1} 1.${ENVT}.vpn
${IP_P2} 2.${ENVT}.vpn
${IP_P3} 3.${ENVT}.vpn
# ${ENVT}, ${ENVT}.vpn
${HOSTS_STRING}
# stand-alone containers, ${ENVT}.vpn" \ |
sudo tee -a /etc/hosts >/dev/null

# Search reachable host
ENVT_HOST=0
ENVT_HOST_FOUND=0
# Set variable for ping with arguments according to platform
if [[ "$OSTYPE" == "linux-gnu" ]]
then
    pingx="ping -c1 -w2"
elif [[ "$OSTYPE" == "darwin"* ]]
then
    pingx="ping -c1 -t2"
fi

for LOOP_ENVT_HOST in  "1.${ENVT}.vpn" "2.${ENVT}.vpn" "3.${ENVT}.vpn"
do
    echo "*** (Pinging a host and check ssh service):" ${LOOP_ENVT_HOST}
    $pingx ${LOOP_ENVT_HOST};
    echo
    PING_RESULT=$?;
    echo "check ssh service" | ssh -o StrictHostKeyChecking=no -o ConnectTimeout=${SSH_CONNECT_TIMEOUT} -p${SSH_PORT} ${SSH_USER}@${LOOP_ENVT_HOST} sh;
    SSH_CONNECT_RESULT=$?;
    if [ ${PING_RESULT} -eq 0 ] && [ ${SSH_CONNECT_RESULT} -eq 0 ]; then
        ENVT_HOST=${LOOP_ENVT_HOST};
        ENVT_HOST_FOUND=1
        break;
    else
        echo "*** [WARNING] This host is unreachable:" ${LOOP_ENVT_HOST}; echo;
    fi
done
echo

if [ "${ENVT_HOST_FOUND}" -eq "0" ]; then
    echo "*** [ERROR] There is no available host to connect to! Exiting."
    clear_hosts
    exit 1;
fi
echo
echo "*** (Start connecting to): " ${ENVT_HOST}
echo

# start ssh-based VPN:
# -L ${LOCAL_HOST}:${LOCAL_PORT}:${CONTAINER_NAME}:${CONTAINER_PORT} \
# ${REMOTE_USER}@${REMOTE_IP} -p${REMOTE_PORT}
ssh_connect

# Delete all VPN lines from a /etc/hosts
echo
echo "*** Enter a ROOT password to clear /etc/hosts from temporary VPN hosts (if asked): "
clear_hosts
